/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/FileSys/ImageFS/SparkFS/Codecs/SparkTar/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 1992 David Pilling.  All rights reserved.
 * Use is subject to license terms.
 */
/*->h.cat */

#define UNIXTAR 0
#define ARCTAR  1
#define USTAR   2
#define ACOMMA  3
#define AFLTAR  4

/* Is 'c' an octal digit? */
#define isodigit(c)     ( ((c) >= '0') && ((c) <= '7') )

/* Date/time in Arm csecs past 01-Jan-1900 */
typedef struct
{
 unsigned char t[5];     /* Low byte first - ie. t[0] is low */
} sys_time;

typedef sys_time TIME;

/* File offset or length */
typedef long int off_t;

struct stat
{
 /* Unix equivalent fields */
 unsigned short  st_mode;        /* mode bits */
 short           st_nlink;       /* number of links to file */
 short           st_uid;         /* user id of owner ( = 0 ) */
 short           st_gid;         /* group id of owner ( = 0 ) */
 off_t           st_size;        /* file size in characters */
 time_t          st_mtime;       /* time file last written or created */
 /* Arm file attribute details */
 unsigned char   st_stamp;       /* is the file timestamped? */
 unsigned int    st_load;        /* load address */
 unsigned int    st_exec;        /* execution address */
 unsigned short  st_type;        /* file type */
 unsigned char   st_attribs;     /* file attributes */
 sys_time        st_time;        /* time stamp (Arm format) */
};

extern char arcpath[256];
extern char arcname[256];

extern union record xheader;
extern struct stat xhstat;      /* Stat struct corresponding */

extern _kernel_oserror * setnofiles(archive * arc,int n);
extern _kernel_oserror * writesp2cat(char * name,archive * arc,int fn);
extern _kernel_oserror * writehdr(archive * arc,int fn);
extern _kernel_oserror * updatecat(linkblock * linkb);
extern _kernel_oserror * loadcat(archive * arc);
extern _kernel_oserror * loadarchive(linkblock * linkb);
extern _kernel_oserror * validate(linkblock * linkb);
extern _kernel_oserror * diropen(linkblock * block);

extern  int nextfilenumber(archive * arc);
extern  int maxfilenumber(archive * arc,int * fn);
extern  int emptydir(archive * arc,int n);
extern _kernel_oserror * deletedir(archive * arc,int maxnum);
extern _kernel_oserror * writefilehdr(int fh,int hdrver,char * name);
extern  void calcdirlens(archive * arc);
extern _kernel_oserror * writearcmark(archive * arc);
extern _kernel_oserror * initcat(archive * arc,char * name);

extern _kernel_oserror * deletefile(linkblock * linkb);
extern _kernel_oserror * createfile(linkblock * linkb);
extern _kernel_oserror * createdir(linkblock * linkb);

extern _kernel_oserror * insentry(archive * arc,int fn,int ind,int size);
extern  void             rementry(archive * arc,int fn,int size);
extern _kernel_oserror * renamex(linkblock * linkb);
extern _kernel_oserror * archiveflush(linkblock * block);


extern _kernel_oserror * read_header(union record * header,struct stat * hstat,
                               archive * arc,int * status);

extern void decode_header(union record *header,struct stat *st,int *stdp);


extern _kernel_oserror * writeeofmarker(archive * arc);
extern _kernel_oserror * writeeofmarkerc(archive * arc);
extern _kernel_oserror * parentof(archive * arc,int fn,int * ind);




extern int tareofposn(archive * arc);
extern int tarfilesize(int size);
extern _kernel_oserror * write_header(archive * arc,union record * header);
extern void start_header(union record * header,char *name,struct stat *st,int method);
extern void ustat(heads * hdr,struct stat *buf);
extern _kernel_oserror * writeeofmarker(archive * arc);
extern _kernel_oserror * tarchunk(archive * arc,int fp,int delta);
